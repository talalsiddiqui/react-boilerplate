import { combineReducers } from 'redux'
import Auth, { IAuthState } from './Containers/Auth/reducers'
import {
  firebaseReducer,
  FirebaseReducer,
  FirestoreReducer,
} from 'react-redux-firebase'
import { firestoreReducer } from 'redux-firestore'

interface UserProfile {
  [name: string]: any
}
interface DBSchema {
  [name: string]: any
}
export interface IRootState {
  authState: IAuthState
  firebase: FirebaseReducer.Reducer<UserProfile, DBSchema>
  firestore: FirestoreReducer.Reducer
}

const rootReducer = combineReducers({
  authState: Auth,
  firebase: firebaseReducer,
  firestore: firestoreReducer,
})

export default rootReducer
