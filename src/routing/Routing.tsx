import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Auth from '../Containers/Auth'
import PrivateRoute from './PrivateRouting'
import Home from '../Containers/Home'

const Routing = () => {
  return (
    <Switch>
      <Route exact path="/auth" component={Auth} />
      <PrivateRoute exact path="/" component={Home} />
      <Redirect to="/" />
    </Switch>
  )
}

export default Routing
