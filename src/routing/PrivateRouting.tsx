import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { IRootState } from '../reducers'
import Spinner from '../Components/Spinner'

const PrivateRoute = ({ component: Component, ...rest }: any) => {
  const auth = useSelector((state: IRootState) => state.firebase.auth)

  return (
    <Route
      {...rest}
      render={props => {
        if (isLoaded(auth) && !isEmpty(auth) && auth.uid) {
          return <Component {...props} />
        }

        if (!auth.isLoaded) {
          return <Spinner size="large" />
        }
        return (
          <Redirect
            to={{
              pathname: '/auth',
              state: { from: props.location },
            }}
          />
        )
      }}
    />
  )
}
export default PrivateRoute
