import React from 'react'
import { Form, Input, Button, DatePicker } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import styles from './index.module.scss'

interface IProps {
  onClickLogin: () => any
  submitedData: any
  signUpProgress: boolean
}

const SignUp = ({ onClickLogin, submitedData, signUpProgress }: IProps) => {
  const onFinish = (fieldsValue: any) => {
    const values = {
      ...fieldsValue,
      dateOfBirth: fieldsValue.dateOfBirth.format('YYYY-MM-DD'),
    }
    console.log('Received values of form: ', values)
    submitedData(values)
  }
  const clickSignUp = () => {
    onClickLogin()
  }
  //  const dateFormat = 'YYYY/MM/DD'

  return (
    <Form
      name="signUpForm"
      className={styles.loginForm}
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        className={styles.formItem}
        name="email"
        rules={[
          { required: true, message: 'Please input your Email!' },
          { type: 'email', message: 'Not a validate email!' },
        ]}
      >
        <Input
          size="large"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Email"
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="password"
        rules={[
          { required: true, message: 'Please input your Password!' },
          { min: 6, message: 'Minimm 6 characters' },
        ]}
      >
        <Input
          size="large"
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="userName"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input
          size="large"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Username"
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="firstName"
        rules={[{ required: true, message: 'Please input your first name!' }]}
      >
        <Input
          size="large"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="First Name"
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="lastName"
        rules={[{ required: true, message: 'Please input your last name!' }]}
      >
        <Input
          size="large"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Last Name"
        />
      </Form.Item>
      <Form.Item
        className={styles.formItem}
        name="dateOfBirth"
        rules={[
          {
            validator(rule, value) {
              if (value) {
                const birthData = value.format('YYYY-MM-DD')

                const year = parseInt(birthData.substring(0, 4), 10)
                const month = parseInt(birthData.substring(5, 7), 10)
                const day = parseInt(birthData.substring(8, 10), 10)

                const today = new Date()
                var birthday = new Date(year, month - 1, day)
                var differenceInMilisecond =
                  today.valueOf() - birthday.valueOf()

                var yearAge = Math.floor(differenceInMilisecond / 31536000000)

                if (yearAge >= 18) {
                  return Promise.resolve()
                }
                return Promise.reject(
                  new TypeError('You are under 18 years old'),
                )
              } else {
                return Promise.reject(
                  new TypeError('Please select your Date of birth!'),
                )
              }
            },
          },
        ]}
      >
        <DatePicker
          style={{ width: '100%' }}
          // format={dateFormat}
          placeholder="Select Date of birth"
        />
      </Form.Item>

      <Form.Item>
        <Button
          type="primary"
          size="large"
          block
          htmlType="submit"
          className="login-form-button"
          shape="round"
          loading={signUpProgress}
        >
          SignUp
        </Button>
      </Form.Item>
      <div className={styles.links}>
        <h3 className={styles.noAccountText}>Already have an account?</h3>
        <h3 className={styles.login} onClick={clickSignUp}>
          Login
        </h3>
      </div>
    </Form>
  )
}

export default SignUp
