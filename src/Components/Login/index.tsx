import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Modal, message, Typography } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import styles from './index.module.scss'

const { Text } = Typography

interface IProps {
  onClickSign: () => any
  submitedData: any
  loginProgress: boolean
  forgetPassword: (email: string) => void
  forgetPasswordProgress: boolean
  forgetPasswordErrorMessage: undefined | string
  forgetPasswordSuccess: boolean
}

const Login = ({
  onClickSign,
  loginProgress,
  submitedData,
  forgetPasswordProgress,
  forgetPassword,
  forgetPasswordErrorMessage,
  forgetPasswordSuccess,
}: IProps) => {
  const [forgetPasswordVisible, setForgetPasswordVisible] = useState(false)
  const [emailID, setEmailID] = useState<undefined | string>(undefined)

  useEffect(() => {
    if (!forgetPasswordProgress && forgetPasswordSuccess) {
      message.success('Email is sent. kindly follow the instructions in email.')
      setForgetPasswordVisible(false)
    }
  }, [forgetPasswordSuccess, forgetPasswordProgress])

  const onFinish = (values: any) => {
    console.log('Received values of form: ', values)
    submitedData(values)
  }
  const clickSignUp = () => {
    onClickSign()
  }

  const handleOk = (e: any) => {
    console.log(e)
    emailID && forgetPassword(emailID)
    // emailID && setForgetPasswordVisible(false)
  }

  const handleCancel = () => {
    setForgetPasswordVisible(false)
  }

  const onForgetPasswordEmailChange = (e: any) => {
    const email = e.target.value
    setEmailID(email)
  }
  return (
    <>
      <Form
        name="loginForm"
        className={styles.loginForm}
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          className={styles.formItem}
          name="email"
          rules={[
            { required: true, message: 'Please input your Email!' },
            { type: 'email', message: 'Not a validate email!' },
          ]}
        >
          <Input
            size="large"
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          className={styles.formItem}
          name="password"
          rules={[
            { required: true, message: 'Please input your Password!' },
            { min: 6, message: 'Minimm 6 characters' },
          ]}
        >
          <Input
            size="large"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            size="large"
            block
            loading={loginProgress}
            htmlType="submit"
            className="login-form-button"
            shape="round"
          >
            Login
          </Button>
        </Form.Item>
        <div className={styles.links}>
          <h3 className={styles.noAccountText}>Dont have an account yet</h3>
          <h3 className={styles.signUp} onClick={clickSignUp}>
            Sign Up
          </h3>
          <h4
            className={styles.forgetPassword}
            onClick={() => setForgetPasswordVisible(true)}
          >
            Forget Password
          </h4>
        </div>
      </Form>
      <Modal
        title="Title"
        visible={forgetPasswordVisible}
        onOk={handleOk}
        confirmLoading={forgetPasswordProgress}
        onCancel={handleCancel}
      >
        <p>
          <Input
            value={emailID}
            autoFocus
            placeholder="Write Email ID"
            onChange={onForgetPasswordEmailChange}
          />
          {forgetPasswordErrorMessage && (
            <Text type="danger">{forgetPasswordErrorMessage}</Text>
          )}
        </p>
      </Modal>
    </>
  )
}

export default Login
