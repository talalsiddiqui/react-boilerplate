import React from 'react'
import { PageHeader, Button, Descriptions } from 'antd'
import { useSelector } from 'react-redux'
import { IRootState } from '../../reducers'
import styles from './index.module.scss'
import { useFirebase } from 'react-redux-firebase'

const Home = () => {
  const firebase = useFirebase()

  const userProfile = useSelector((state: IRootState) => state.firebase.profile)
  const { dateOfBirth, email, lastName, firstName, userName } = userProfile

  console.log('userProfile', userProfile)
  const logout = () => {
    firebase.logout()
  }

  return (
    <div className={styles.home}>
      <PageHeader
        ghost={false}
        className="site-page-header"
        title="Home Page"
        extra={[
          <Button key="1" type="primary" onClick={logout}>
            Logout
          </Button>,
        ]}
      >
        <Descriptions size="small" column={2}>
          <Descriptions.Item label="Name">{userName}</Descriptions.Item>
          <Descriptions.Item label="Email">{email}</Descriptions.Item>
          <Descriptions.Item label="First Name">{firstName}</Descriptions.Item>
          <Descriptions.Item label="Last Name">{lastName}</Descriptions.Item>
          <Descriptions.Item label="Date OF Birth">
            {dateOfBirth}
          </Descriptions.Item>
        </Descriptions>
      </PageHeader>
    </div>
  )
}

export default Home
