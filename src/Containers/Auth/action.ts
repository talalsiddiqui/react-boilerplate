export enum ActionTypes {
  POST_SIGNUP_PROGRESS = 'auth/POST_SIGNUP_PROGRESS',
  POST_SIGNUP_SUCCESS = 'auth/POST_SIGNUP_SUCCESS',
  POST_SIGNUP_FAILURE = 'auth/POST_SIGNUP_FAILURE',
  LOGIN_PROGRESS = 'auth/LOGIN_PROGRESS',
  LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS',
  LOGIN_FAILURE = 'auth/LOGIN_FAILURE',
  FORGET_PASSWORD_PROGRESS = 'auth/FORGET_PASSWORD_PROGRESS',
  FORGET_PASSWORD_SUCCESS = 'auth/FORGET_PASSWORD_SUCCESS',
  FORGET_PASSWORD_FAILURE = 'auth/FORGET_PASSWORD_FAILURE',
}
export interface PostSignUpProgress {
  type: ActionTypes.POST_SIGNUP_PROGRESS
  payload: any
}

export interface PostSignUpSuccess {
  type: ActionTypes.POST_SIGNUP_SUCCESS
}

export interface PostSignUpFailure {
  type: ActionTypes.POST_SIGNUP_FAILURE
  payload: any
}
export interface LoginProgress {
  type: ActionTypes.LOGIN_PROGRESS
  payload: any
}

export interface LoginSuccess {
  type: ActionTypes.LOGIN_SUCCESS
}

export interface LoginFailure {
  type: ActionTypes.LOGIN_FAILURE
  payload: any
}
export interface ForgetPasswordProgress {
  type: ActionTypes.FORGET_PASSWORD_PROGRESS
  payload: any
}

export interface ForgetPasswordSuccess {
  type: ActionTypes.FORGET_PASSWORD_SUCCESS
}

export interface ForgetPasswordFailure {
  type: ActionTypes.FORGET_PASSWORD_FAILURE
  payload: any
}

function postSignUpProgress(payload: any): PostSignUpProgress {
  return {
    type: ActionTypes.POST_SIGNUP_PROGRESS,
    payload,
  }
}

function postSignUpSuccess(): PostSignUpSuccess {
  return {
    type: ActionTypes.POST_SIGNUP_SUCCESS,
  }
}

function postSignUpFailure(payload: any): PostSignUpFailure {
  return {
    type: ActionTypes.POST_SIGNUP_FAILURE,
    payload,
  }
}
function loginProgress(payload: any): LoginProgress {
  return {
    type: ActionTypes.LOGIN_PROGRESS,
    payload,
  }
}

function loginSuccess(): LoginSuccess {
  return {
    type: ActionTypes.LOGIN_SUCCESS,
  }
}

function loginFailure(payload: any): LoginFailure {
  return {
    type: ActionTypes.LOGIN_FAILURE,
    payload,
  }
}

function forgetPasswordProgress(payload: any): ForgetPasswordProgress {
  return {
    type: ActionTypes.FORGET_PASSWORD_PROGRESS,
    payload,
  }
}

function forgetPasswordSuccess(): ForgetPasswordSuccess {
  return {
    type: ActionTypes.FORGET_PASSWORD_SUCCESS,
  }
}
function forgetPasswordFailure(payload: any): ForgetPasswordFailure {
  return {
    type: ActionTypes.FORGET_PASSWORD_FAILURE,
    payload,
  }
}

export type Action =
  | PostSignUpProgress
  | PostSignUpFailure
  | PostSignUpSuccess
  | LoginFailure
  | LoginProgress
  | LoginSuccess
  | ForgetPasswordSuccess
  | ForgetPasswordProgress
  | ForgetPasswordFailure

export const Actions = {
  postSignUpProgress,
  postSignUpSuccess,
  postSignUpFailure,
  loginProgress,
  loginSuccess,
  loginFailure,
  forgetPasswordProgress,
  forgetPasswordSuccess,
  forgetPasswordFailure,
}
